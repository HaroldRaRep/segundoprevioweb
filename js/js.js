function cargarClientes() {
    fetch("./json/jsonClientes.json")
      .then(response => response.json())
      .then(data => {
        const select = document.getElementById("seleccionarCliente");
        data.Personas.forEach(persona => {
          const option = document.createElement("option");
          option.value = persona.Nombre;
          option.text = persona.Nombre;
          select.appendChild(option);
        });
      })
      .catch(error => console.error(error));
  }


  function createClientProfile() {
    var clienteSelect = document.getElementById('seleccionarCliente');
    var categoriaSelect = document.getElementById('seleccionarCategorias');
    var clienteId = clienteSelect.value;
    var clienteNombre = clienteSelect.options[clienteSelect.selectedIndex].text;
    var categoriaSeleccionada = categoriaSelect.value;

    // Guardar los valores en el LocalStorage
    localStorage.setItem('clienteId', clienteId);
    localStorage.setItem('clienteNombre', clienteNombre);
    localStorage.setItem('categoriaSeleccionada', categoriaSeleccionada);

    // Redirigir a la página clientes.html
    window.location.href = 'html/clientes.html';
  }
